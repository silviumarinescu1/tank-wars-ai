module.exports = {
    API_KEY: '',
    APP_ID: '',
    AUTH_DOMAIN: '',
    DATABASE_URL: '',
    FIREBASE_TOKEN: '',
    MEASUREMENT_ID: '',
    MESSAGING_SENDER_ID: '',
    PROJECT_ID: '',
    STORAGE_BUCKET: '',
  };