### USAGE ###

* npm i
* npm run dev


### WHAT IS NOT DONE ###
* front-end should emit events when tank location on map changes or bullet location chages
* back end should respond to the above events with new events based on AI logic
* back end should decide when game over