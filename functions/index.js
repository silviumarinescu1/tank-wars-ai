const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();

exports.eventTriguer = functions.firestore
  .document("games/{gameId}/events/{eventId}")
  .onCreate((snap, context) => {
    const event = snap.data();
    if (event.type == "gameStart") {
      // let state = event.payload.initialState;
      return mooveTankEvent(0, 0, context.params.gameId);
    }
    if (event.type == "mooveTank") {
      return db
        .doc(`games/${context.params.gameId}`)
        .get()
        .then(stateRef => {
          const state = stateRef.data();
          state.tanks[event.payload.tank].direction = event.payload.direction;
          return db.doc(`games/${context.params.gameId}`).update(state);
        });
    }
    if (event.type == "fireTank") {
        return db
          .doc(`games/${context.params.gameId}`)
          .get()
          .then(stateRef => {
            const state = stateRef.data();
            state.tanks[event.payload.tank].fire = true;
            return db.doc(`games/${context.params.gameId}`).update(state);
          });
      }
    return;
  });

const mooveTankEvent = (tank, direction, gameId) => {
  return db.collection(`games/${gameId}/events`).add({
    type: "mooveTank",
    payload: {
      tank,
      direction
    },
    date: new Date()
  });
};

const fireTankEvent = (tank, direction, gameId) => {
    return db.collection(`games/${gameId}/events`).add({
      type: "fireTank",
      payload: {
        tank
      },
      date: new Date()
    });
  };
