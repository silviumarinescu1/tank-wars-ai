import game from "./game";
import resources from "./resources";
import state from "./state";
import tank from "./tank";

const getAngle = direction =>
  direction === 0 ? 180 : direction === 1 ? 270 : direction === 2 ? 0 : 90;

const getNextLocation = (location, direction) => [
  location[0] + (direction === 0 ? 1 : direction === 2 ? -1 : 0),
  location[1] + (direction === 1 ? 1 : direction === 3 ? -1 : 0)
];

export default {
  renderFire: (localTank, timeDiff) => {
    let localState = state.getState();
    if (
      localState.tanks[localTank].fire &&
      localState.tanks[localTank].state === 3
    ) {
      localState.tanks[localTank].fireProcentaje +=
        timeDiff / game.getSettings().tanks[localTank].fireSpeed;
      const nextLoc = tank.getNextLocation(localTank);
      if (localState.tanks[localTank].fireProcentaje >= 1) {
        localState.tanks[localTank].fireProcentaje = 0;
        localState.tanks[localTank].state = 0;
        localState.tanks[localTank].fire = false;
        localState.map[nextLoc[1]][nextLoc[0]] = 9;
        localState.bullets.push({
          location: nextLoc,
          direction: localState.tanks[localTank].currentDirection,
          tank: localTank,
          progress: 0
        });
      } else {
        const prevLoc = localState.tanks[localTank].location.slice();
        const loc = [0, 0];
        loc[0] = prevLoc[0] + (nextLoc[0] - prevLoc[0]) * 0.5;
        loc[1] = prevLoc[1] + (nextLoc[1] - prevLoc[1]) * 0.5;
        game
          .getCtx()
          .drawImage(
            resources.fire.src,
            ...resources.fire.frames[
              Math.floor(
                resources.fire.frames.length *
                  localState.tanks[localTank].fireProcentaje
              )
            ],
            game.getSquareSize()[0] * loc[0],
            game.getSquareSize()[1] * loc[1],
            ...game.getSquareSize()
          );
      }
    }
    state.setState(localState);
  },
  renderBullets: timeDiff => {
    let localState = state.getState();
    if (localState.bullets.length > 0) {
      localState.bullets.forEach((bullet, i) => {
        localState.bullets[i].progress +=
          timeDiff / game.getSettings().tanks[bullet.tank].bulletSpeed;
        let bulletExists = true;
        if (localState.bullets[i].progress >= 1) {
          localState.bullets[i].progress = 0;
          bullet.location = getNextLocation(bullet.location, bullet.direction);
          if (
            bullet.location[0] ==
              localState.tanks[bullet.tank == 0 ? 1 : 0].location[0] &&
            bullet.location[1] ==
              localState.tanks[bullet.tank == 0 ? 1 : 0].location[1]
          ) {
            game.stop(bullet.tank == 0 ? 1 : 0);
          }
          if (
            bullet.location[0] >= localState.map[0].length ||
            bullet.location[1] >= localState.map.length ||
            bullet.location[1] <= -1 ||
            bullet.location[0] <= -1 ||
            localState.map[bullet.location[1]][bullet.location[0]] == 3
          ) {
            localState.bullets.splice(i, 1);
            bulletExists = false;
          }
        }

        if (bulletExists) {
          const center = [
            game.getSquareSize()[0] * bullet.location[0],
            game.getSquareSize()[1] * bullet.location[1]
          ];
          center[0] = Math.round(center[0] + game.getSquareSize()[0] / 2);
          center[1] = Math.round(center[1] + game.getSquareSize()[1] / 2);
          game.getCtx().translate(...center);
          game.getCtx().rotate((getAngle(bullet.direction) * Math.PI) / 180);
          game.getCtx().translate(...center.map(p => p * -1));
          game.getCtx().drawImage(
            resources.bullet.src,
            ...resources.bullet.pos,
            game.getSquareSize()[0] * bullet.location[0] +
              game.getSquareSize()[0] * 0.3 +
              game.getSquareSize()[0] * bullet.progress * -1,

            game.getSquareSize()[1] * bullet.location[1] +
              game.getSquareSize()[1] * 0.3,
            ...game.getSquareSize().map(p => p * 0.4)
          );
          game.getCtx().setTransform(1, 0, 0, 1, 0, 0);
        }
      });
    }
    state.setState(localState);
  }
};
