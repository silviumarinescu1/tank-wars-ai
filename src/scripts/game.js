import state from "./state";
import land from "./land";
import obstacles from "./obstacles";
import tank from "./tank";
import bullet from "./bullet";
let settings = {};
let canvas = null;
let ctx = null;
let width = window.innerWidth;
let height = window.innerHeight;
let squareSize = [];
let lastTime = 0;
let gameState = 0;

const render = timeStamp => {
  if (width !== window.innerWidth) {
    width = window.innerWidth;
    canvas.width = width;
    squareSize[0] = Math.round(window.innerWidth / settings.map[0].length);
  }
  if (height !== window.innerHeight) {
    height = window.innerHeight;
    canvas.height = height;
    squareSize[1] = Math.round(window.innerHeight / settings.map.length);
  }
  renderFrame(timeStamp - lastTime);
  lastTime = timeStamp;
  if (gameState)
    setTimeout(() => {
      requestAnimationFrame(render);
    }, settings.fps);
};

const renderFrame = timeDiff => {
  land.render();
  obstacles.render();
  tank.render(0, timeDiff);
  tank.render(1, timeDiff);
  bullet.renderBullets(timeDiff);
};

window.addEventListener("keydown", e => {
  let localState = state.getState();
  if (localState.mode !== "player") return;
  switch (e.keyCode) {
    case 32: //space
      localState.tanks[1].fire = true;
      break;
    case 13: //enter
      localState.tanks[0].fire = true;
      break;
    case 37: //left
      localState.tanks[0].direction = 2;
      break;
    case 38: //up
      localState.tanks[0].direction = 3;
      break;
    case 39: //right
      localState.tanks[0].direction = 0;
      break;
    case 40: //down
      localState.tanks[0].direction = 1;
      break;
    case 65: //left
      localState.tanks[1].direction = 2;
      break;
    case 87: //up
      localState.tanks[1].direction = 3;
      break;
    case 68: //right
      localState.tanks[1].direction = 0;
      break;
    case 83: //down
      localState.tanks[1].direction = 1;
      break;
  }
  state.setState(localState);
});
let unsubscribe = () => {};
const initTank = localTank => {
  let localState = state.getState();
  for (let i = 0; i < localState.map.length; i++)
    for (let j = 0; j < localState.map[0].length; j++)
      if (localState.map[i][j] === localTank + 1)
        localState.tanks[localTank].location = [j, i];

  localState.tanks[localTank].direction = settings.tanks[localTank].direction;
  localState.tanks[localTank].currentDirection =
    settings.tanks[localTank].direction;
  localState.tanks[localTank].lastDirection =
    settings.tanks[localTank].direction;
  state.setState(localState);
};
let end = null;
let gameId = null;
export default {
  getCanvas: () => canvas,
  getCtx: () => ctx,
  getWidth: () => width,
  getHeight: () => height,
  getSquareSize: () => squareSize,
  getSettings: () => settings,
  getGameId: () => gameId,
  start: (mode, localSettings, firebase, db, onEnd) => {
    localSettings.map = localSettings.map.map(row =>
      row.split(",").map(cell => parseInt(cell.trim()))
    );
    settings = localSettings;
    squareSize = [
      Math.round(window.innerWidth / settings.map[0].length),
      Math.round(window.innerHeight / settings.map.length)
    ];
    end = onEnd;
    let localState = state.getState();
    localState.map = settings.map.slice();
    localState.mode = mode;
    state.setState(localState);
    initTank(0);
    initTank(1);
    gameState = 1;
    canvas = document.getElementById("canvas");
    canvas.width = width;
    canvas.height = height;
    ctx = canvas.getContext("2d");
    localState = state.getState();
    if (mode === "ai")
      db.collection("games")
        .add({ ...localState, map: localState.map.map(col => col.join(", ")) })
        .then(gameRef => {
          gameId = gameRef.id;
          db.collection(`games/${gameId}/events`)
            .add({
              type: "gameStart",
              payload: {
                initialState: {
                  ...localState,
                  map: localState.map.map(col => col.join(", "))
                }
              },
              date: firebase.firestore.FieldValue.serverTimestamp()
            })
            .then(() => {
              unsubscribe = state.onChange(db, gameId);
              window.requestAnimationFrame(render);
            });
        });
    else window.requestAnimationFrame(render);
  },
  stop: tank => {
    unsubscribe();
    end(tank);
    gameState = 0;
  }
};
