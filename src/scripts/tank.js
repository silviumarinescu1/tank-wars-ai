import game from "./game";
import bullet from "./bullet";
import resources from "./resources";
import state from "./state";

const getNextLocation = tank => {
  let localState = state.getState();
  return [
    localState.tanks[tank].location[0] +
      (localState.tanks[tank].currentDirection === 0
        ? 1
        : localState.tanks[tank].currentDirection === 2
        ? -1
        : 0),
    localState.tanks[tank].location[1] +
      (localState.tanks[tank].currentDirection === 1
        ? 1
        : localState.tanks[tank].currentDirection === 3
        ? -1
        : 0)
  ];
};

const chagenTankLocation = tank => {
  let localState = state.getState();
  localState.map[localState.tanks[tank].location[1]][
    localState.tanks[tank].location[0]
  ] = 0;
  localState.tanks[tank].location = getNextLocation(tank);
  if (
    localState.tanks[tank].location[0] >= localState.map[0].length ||
    localState.tanks[tank].location[1] >= localState.map.length ||
    localState.tanks[tank].location[0] <= -1 ||
    localState.tanks[tank].location[1] <= -1
  )
    game.stop(tank);
  else {
    if (
      localState.map[localState.tanks[tank].location[1]][
        localState.tanks[tank].location[0]
      ] == 3
    ) {
      game.stop(tank);
    }
    localState.map[localState.tanks[tank].location[1]][
      localState.tanks[tank].location[0]
    ] = tank + 1;
  }
  state.setState(localState);
};
const getAngle = direction =>
  direction === 0 ? 0 : direction === 1 ? 90 : direction === 2 ? 180 : 270;

const getDiffAngle = tank => {
  let localState = state.getState();
  let initialAngle = getAngle(localState.tanks[tank].currentDirection);
  let destinalionAngle = getAngle(localState.tanks[tank].lastDirection);

  if (Math.abs(destinalionAngle - initialAngle) > 180)
    if (destinalionAngle > initialAngle) initialAngle += 360;
    else destinalionAngle += 360;

  return destinalionAngle - initialAngle;
};

const mooveTank = (tank, timeDiff) => {
  let localState = state.getState();
  if (localState.tanks[tank].state == 0) {
    localState.tanks[tank].mooveProcentaje +=
      timeDiff / game.getSettings().tanks[tank].speed;
    if (localState.tanks[tank].mooveProcentaje >= 1) {
      if (
        localState.tanks[tank].currentDirection !==
        localState.tanks[tank].direction
      ) {
        localState.tanks[tank].lastDirection = localState.tanks[tank].direction;
        localState.tanks[tank].state = 1;
      } else {
        if (localState.tanks[tank].fire) {
          localState.tanks[tank].state = 3;
        } else {
          localState.tanks[tank].mooveProcentaje = 0;
          state.setState(localState);
          chagenTankLocation(tank);
          localState = state.getState();
        }
      }
    }
  }
  state.setState(localState);
  return [
    localState.tanks[tank].location[0] * game.getSquareSize()[0] -
      game.getSquareSize()[0] *
        (1 - localState.tanks[tank].mooveProcentaje) *
        (localState.tanks[tank].currentDirection === 0
          ? 1
          : localState.tanks[tank].currentDirection === 2
          ? -1
          : 0),
    localState.tanks[tank].location[1] * game.getSquareSize()[1] -
      game.getSquareSize()[1] *
        (1 - localState.tanks[tank].mooveProcentaje) *
        (localState.tanks[tank].currentDirection === 1
          ? 1
          : localState.tanks[tank].currentDirection === 3
          ? -1
          : 0)
  ];
};

const rotateTank = (tank, timeDiff) => {
  let localState = state.getState();
  if (localState.tanks[tank].state == 1) {
    localState.tanks[tank].rotateProcentaje +=
      timeDiff /
      ((game.getSettings().tanks[tank].rotationSpeed *
        Math.abs(getDiffAngle(tank))) /
        360);
    if (localState.tanks[tank].rotateProcentaje >= 1) {
      if (
        localState.tanks[tank].lastDirection ===
        localState.tanks[tank].direction
      ) {
        localState.tanks[tank].rotateProcentaje = 0;
        localState.tanks[tank].state = 0;
        localState.tanks[tank].currentDirection =
          localState.tanks[tank].lastDirection;
        localState.tanks[tank].mooveProcentaje = 0;
        state.setState(localState);
        chagenTankLocation(tank);
        localState = state.getState();
      } else {
        localState.tanks[tank].rotateProcentaje = 0;
        localState.tanks[tank].currentDirection =
          localState.tanks[tank].lastDirection;
        localState.tanks[tank].lastDirection = localState.tanks[tank].direction;
      }
    }
  }
  let rotateAngel =
    localState.tanks[tank].defaulRotation +
    getAngle(localState.tanks[tank].currentDirection);

  if (localState.tanks[tank].state === 1)
    rotateAngel += getDiffAngle(tank) * localState.tanks[tank].rotateProcentaje;
  state.setState(localState);
  return rotateAngel;
};
let firstTime = [true, true];
export default {
  render: (tank, timeDiff) => {
    if (firstTime[tank]) {
      firstTime[tank] = false;
      chagenTankLocation(tank);
    }
    let newPos = mooveTank(tank, timeDiff);
    let rotateAngel = rotateTank(tank, timeDiff);
    const center = newPos.slice();
    center[0] = Math.round(center[0] + game.getSquareSize()[0] / 2);
    center[1] = Math.round(center[1] + game.getSquareSize()[1] / 2);
    game.getCtx().translate(...center);
    game.getCtx().rotate((rotateAngel * Math.PI) / 180);
    game.getCtx().translate(...center.map(p => p * -1));
    game
      .getCtx()
      .drawImage(
        resources.tanks[tank].src,
        ...resources.tanks[tank].pos,
        ...newPos,
        ...game.getSquareSize()
      );
    game.getCtx().setTransform(1, 0, 0, 1, 0, 0);
    bullet.renderFire(tank, timeDiff);
  },
  getNextLocation
};
