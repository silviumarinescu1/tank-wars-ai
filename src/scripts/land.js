import game from "./game";
import resources from "./resources";

let pattern = null;

export default {
  render: () => {
    game.getCtx().beginPath();
    game.getCtx().rect(0, 0, game.getWidth(), game.getHeight());
    if (!pattern)
      pattern = game.getCtx().createPattern(resources.land.src, "repeat");
    game.getCtx().fillStyle = pattern;
    game.getCtx().fill();
  }
};
