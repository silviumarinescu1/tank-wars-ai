import game from "./game";
import state from "./state";
import resources from "./resources";

export default {
  render: () => {
    let localState = state.getState();
    for (let i = 0; i < localState.map.length; i++)
      for (let j = 0; j < localState.map[0].length; j++)
        if (localState.map[i][j] === 3)
          game.getCtx().drawImage(
            resources.obstacle.src,
            ...resources.obstacle.pos,
            game.getSquareSize()[0] * j,
            game.getSquareSize()[1] * i,
            ...game.getSquareSize()
          );
  }
};
