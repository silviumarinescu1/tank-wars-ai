let state = {
  bullets: [],
  tanks: [
    {
      state: 0,
      fire: false,
      fireProcentaje: 0,
      mooveProcentaje: 0,
      rotateProcentaje: 0,
      defaulRotation: 0
    },
    {
      state: 0,
      fire: false,
      fireProcentaje: 0,
      mooveProcentaje: 0,
      rotateProcentaje: 0,
      defaulRotation: 180
    }
  ]
};

export default {
  onChange: (db, gameId) =>
    db.doc(`games/${gameId}`).onSnapshot(doc => {
      const localState = doc.data();
      state.tanks[0].direction = localState.tanks[0].direction;
      state.tanks[1].direction = localState.tanks[1].direction;
      state.tanks[0].fire = localState.tanks[0].fire;
      state.tanks[1].fire = localState.tanks[1].fire;
    }),
  getState: () => state,
  setState: newSate => {
    state = newSate;
  }
};
