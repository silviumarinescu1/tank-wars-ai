import landSrc from "../images/land.png";
import tanksSrc from "../images/tanks.png";
import obstacleSrc from "../images/obstacle.png";
import spriteSrc from "../images/sprite.png";
const land = document.createElement("img");
land.src = landSrc;
const tank = document.createElement("img");
tank.src = tanksSrc;
const obstacle = document.createElement("img");
obstacle.src = obstacleSrc;
const sprite = document.createElement("img");
sprite.src = spriteSrc;

export default {
  land: {
    src: land
  },
  fire: {
    src: sprite,
    frames: [
       [0, 117, 39, 39],
       [39, 117, 39, 39],
       [78, 117, 39, 39],
       [117, 117, 39, 39],
       [156, 117, 39, 39],
       [195, 117, 39, 39],
       [234, 117, 39, 39],
       [273, 117, 39, 39],
       [312, 117, 39, 39],
       [351, 117, 39, 39],
       [390, 117, 39, 39],
       [429, 117, 39, 39],
       [468, 117, 39, 39],
    ]
  },
  bullet:{
    src:sprite,
    pos: [0, 78, 78, 38]
  },
  tanks: [
    {
      src: tank,
      pos: [55, 76, 193, 133]
    },
    {
      src: tank,
      pos: [368, 76, 193, 133]
    }
  ],
  obstacle: {
    src: obstacle,
    pos: [417, 512, 220, 220]
  }
};
